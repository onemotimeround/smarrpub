import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

// TODO If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

const GSTATES     = { ROLL: 'Roll', MOVE: 'Move', SKIP: 'Skip', COMPLETE: 'Complete' }
const PCOLORS     = ['#ffffff', '#4CAF50', '#008CBA', '#ffff00', '#555555' ]
const DIEVALTXT   = ['\u{1f3b2}', '\u2680', '\u2681', '\u2682', '\u2683', '\u2684', '\u2685']
const GOALSTART   = [0, 65, 69, 73, 77]
const HOMESTART   = [0, 49, 53, 57, 61]
const PATHSTART   = [0,  0, 12, 24, 36]
const PATHEND     = [0, 46, 10, 22, 34]
const ARROWS      = new Map([[0,'\u21D2'], [12,'\u21D3'], [24,'\u21D0'], [36,'\u21D1'], [4,'\u21D8'], [16,'\u21D9'], [28,'\u21D6'], [40,'\u21D7'], [5,'\u21D1'], [17,'\u21D2'], [29,'\u21D3'], [41,'\u21D0']])
const CORNERS     = [4, 16, 28, 40]
const FILLCHAR    = 0
const MTG         = -1  // empty grid square val
const ROWCOLCOUNT = 13
const MAXPATHIDX  = 47
const CENTER      = 48
const MAXSQUARES  = 81;    //  0-48 (49) board path + 4 * 2(home, goal) * 4 (max players) = 49 + 32

//               0     1     2     3     4     5     6     7     8     9     10    11    12
const GRID  = [ 49,  MTG,  MTG,   MTG,    8,    9,   10,   11,   12,  MTG,  MTG,  MTG,   53,
                MTG,  50,  MTG,   MTG,    7,  MTG,   69,  MTG,   13,  MTG,  MTG,   54,  MTG,
                MTG,  MTG,  51,   MTG,    6,  MTG,   70,  MTG,   14,  MTG,   55,  MTG,  MTG,
                MTG,  MTG,  MTG,   52,    5,  MTG,   71,  MTG,   15,   56,  MTG,  MTG,  MTG,
                  0,    1,    2,    3,    4,  MTG,   72,  MTG,   16,   17,   18,   19,   20,
                 47,  MTG,  MTG,  MTG,  MTG,  MTG,  MTG,  MTG,  MTG,  MTG,  MTG,  MTG,   21,
                 46,   65,   66,   67,   68,  MTG,   48,  MTG,   76,   75,   74,   73,   22,
                 45,  MTG,  MTG,  MTG,  MTG,  MTG,  MTG,  MTG,  MTG,  MTG,  MTG,  MTG,   23,
                 44,   43,   42,   41,   40,  MTG,   80,  MTG,   28,   27,   26,   25,   24,
                MTG,  MTG,  MTG,   64,   39,  MTG,   79,  MTG,   29,   60,  MTG,  MTG,  MTG,
                MTG,  MTG,   63,  MTG,   38,  MTG,   78,  MTG,   30,  MTG,   59,  MTG,  MTG,
                MTG,   62,  MTG,  MTG,   37,  MTG,   77,  MTG,   31,  MTG,  MTG,   58,  MTG,
                 61,  MTG,  MTG,  MTG,   36,   35,   34,   33,   32,  MTG,  MTG,  MTG,   57
              ]


/**  Board represents the game playing area.
*/
class Board extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            squares:    Array(MAXSQUARES).fill(FILLCHAR),  // the game grid, including home/base/center
            currPlayer: 1,
            dieVal:     0,
            gstate:     GSTATES.ROLL,
            nextMoves:  new Map()                          //  potential moves
        };

        //  initialize the home squares with the player values
        for (let playerNum = 1; playerNum < 5; playerNum++) {
            let pathIdx = HOMESTART[playerNum]
            for (let homeSqrIdx = 0; homeSqrIdx < 4; homeSqrIdx++) {
                this.state.squares[pathIdx++] = playerNum;        //  initialize the home squares with player values
            }
        }
    }  // end Board constructor  ===============================================

    /**
     * createTable builds the full grid with board
     *
     * @return array of elements
     */
    createTable = () => {    //  TODO  CREDIT  https://blog.cloudboost.io/for-loops-in-react-render-no-you-didnt-6c9f4aa73778
        let table = []
        let gridIdx = 0
        for (let i = 0; i < ROWCOLCOUNT; i++) {     // Outer loop to create parent
            let children = []
            for (let j = 0; j < ROWCOLCOUNT; j++) {    //Inner loop to create children
                //  if fillchar, it's unused
                let boardIdx = GRID[gridIdx]
                if (boardIdx === MTG) {
                    children.push(<td key={gridIdx}>{this.unusedSquare()}</td>)
                } else {
                    children.push(<td key={gridIdx}>{this.renderSquare(boardIdx)}</td>)
                }
                gridIdx++
            }
            //Create the parent and add the children
            table.push(<tr key={i} className="board-row">{children}</tr>)
        }
        return table
    }  //  end createTable  ======================================================

    /**
     * unusedSquare renders a square in the grid that is not used
     *
     * @return unused Square element
     */
    unusedSquare() { return (<button className="unusedsquare" disabled>{FILLCHAR}</button>); }

    /**
     * currPlayerSquare renders a square showing the current user's color
     *
     * @return current player Square element
     */
    currPlayerSquare(playerNum) { return (<button className="currPlayer" style={{"backgroundColor":PCOLORS[playerNum], "color":PCOLORS[playerNum]}} disabled>{FILLCHAR}</button>); }

    /**
     * renderSkipSquare renders an element to allow a player to skip their turn
     *
     * @return skip Square element (if skip state), else the game state
     */
    renderSkipSquare() {
        if (this.state.gstate === GSTATES.SKIP) {
            return (<button className="skipSquare" onClick={() => this.handleClick(0)}>{this.state.gstate}</button>)
        } else {
            return this.state.gstate
        }
    }

    /**
     * renderDieSquare renders an element to allow a player to skip their turn
     *
     * @return die Square element
     */
    renderDieSquare() {
        let isRoll = (this.state.gstate === GSTATES.ROLL);  //  disabled if state is not roll
        return (
            <button className="diesquare" onClick={() => this.handleClick(0)} disabled={!isRoll}>{DIEVALTXT[this.state.dieVal]}</button>
        );
    }

    /**
     * renderSquare renders a board square
     *
     * @param brdIdx - the board Index of the square to render
     * @return a board square element
     */
    renderSquare(brdIdx) {
        let sqrVal      = this.state.squares[brdIdx]
        let sqrDisabled = true
        let sqrHiLite   = false
        for (let [k, v] of this.state.nextMoves) {
            if (v === brdIdx) {     //  both to (key) and from (val) are hi-lighted
                sqrHiLite   = true
            }
            if (k === brdIdx) {     //  only to (key) is enabled
                sqrHiLite   = true
                sqrDisabled = false
            }
        }
        // let cIdx        = sqrVal - 1
        let bgClr       = PCOLORS[sqrVal]
        let txtClr      = bgClr
        let cName       = sqrVal === FILLCHAR ? "gsquare" : "psquare"
        cName           += sqrHiLite ? "h" : ""
        let displayVal  = sqrVal === FILLCHAR ? "." : sqrVal.toString()

        //  draw the home and goal squares
        if (brdIdx >= HOMESTART[1] && brdIdx < MAXSQUARES && !cName.includes("psquare")) {

            let idx = brdIdx - HOMESTART[1]       // [4,4,4,4][4,4,4,4]  : home/goal layout in array
            // 0, 1, 2, 3, / 4, 5, 6, 7, / 8, 9, 10, 11 / 12, 13, 14, 15 : mod to 16 same algorithm for home/goal
            // 1             2             3              4              : divide by four to get the playerNum
            let playerNum = Math.floor(((idx % 16) / 4) + 1)
            let cssHomeGoal = PCOLORS[playerNum] + "," + PCOLORS[0]
            cName = "homeGoal" + (sqrHiLite ? "h" : "")
            return (
                <button className={cName} onClick={() => this.handleClick(brdIdx)} style={{"backgroundImage":"radial-gradient(" + cssHomeGoal + ")", "color":PCOLORS[playerNum]}} disabled={sqrDisabled}>{displayVal}</button>
            );
        }

        //  check for start and corner arrows
        if (ARROWS.has(brdIdx) && !cName.includes("psquare")) {
            displayVal = ARROWS.get(brdIdx).normalize()
            txtClr = sqrVal === FILLCHAR ? '#000000' : PCOLORS[sqrVal]
        }
        return (
            <button className={cName} onClick={() => this.handleClick(brdIdx)} style={{"backgroundColor":bgClr, "color":txtClr}} disabled={sqrDisabled}>{displayVal}</button>
        );
    }  //  end renderSquare  =====================================================

    /**
     * showNextMoves finds the next moves on the board.
     *
     * @return a map of legal moves where the target brd sqr is the key and the from brd sqr is the value
     */
    showNextMoves(dieVal, currPlayer, squares) {
        let nextMoves  = new Map()  //  [to, from]
        var countOnBrd = 0
        let pathStart = PATHSTART[currPlayer]
        let pathEnd   = PATHEND[currPlayer]
        let goalStart = GOALSTART[currPlayer]
        let goalEnd   = goalStart + 3
        let nextIdx   = 0

        for(let pathIdx = 0; pathIdx <= MAXPATHIDX; pathIdx++) {
            nextIdx = pathIdx + dieVal
            let modNextIdx = nextIdx % (MAXPATHIDX + 1)

            if (squares[pathIdx] === currPlayer) {
                countOnBrd++
                if (nextIdx > pathEnd && pathIdx <= pathEnd) { //  check if going past goal
                    let overCount = nextIdx - pathEnd            //  how many past goal
                    let goalIdx = goalStart + (overCount - 1)
                    //  need to know if goal is already occupied  don't go past the goal end
                    if (goalIdx <= goalEnd && squares[goalIdx] !== currPlayer) {
                        nextMoves.set(goalIdx, pathIdx)
                    }
                } else if (squares[nextIdx] !== currPlayer) {
                    nextMoves.set(modNextIdx, pathIdx)
                }

                let nextMinusOne = modNextIdx - 1
                if (CORNERS.includes(nextMinusOne) && squares[CENTER] !== currPlayer) {
                    nextMoves.set(CENTER, pathIdx)  //  TODO this is a bug, could collide
                }
            }  // end if player on sqr
        }  //  end for looping through base game board

        if (squares[CENTER] === currPlayer) {            //  moving from center
            countOnBrd++

            for (let idx = 0; idx < 4; idx++) {
                let tgtIdx = CORNERS[idx] + (dieVal - 1)
                if (squares[tgtIdx] !== currPlayer) {
                    nextMoves.set(tgtIdx, CENTER)
                }
            }
        }

        if (countOnBrd < 4) {  //  some are at home, but some may be at goal
            //  need to look for moves inside the goal
            for(let brdIdx = goalStart; brdIdx <= goalEnd; brdIdx++) {
                if (squares[brdIdx] === currPlayer) {
                    countOnBrd++
                    nextIdx = brdIdx + dieVal

                    if (nextIdx <= goalEnd && squares[nextIdx] !== currPlayer) {
                        nextMoves.set(nextIdx, brdIdx)
                    }
                }
                if (countOnBrd < 4) {   //  show move from home
                    let homeSqr = HOMESTART[currPlayer] + countOnBrd
                    if (squares[homeSqr] === currPlayer && squares[pathStart] !== currPlayer
                                                        && (dieVal === 1 || dieVal === 6)) {
                        nextMoves.set(pathStart, homeSqr)
                    }
                }
            }
        }

        return nextMoves;
    }  //  end showNextMoves  =========================================================

    /** sendPieceHome takes an intercepted piece and returns it home
     *
     * @param array board squares
     * @param int index in the board the piece to be moved is on
     */
    sendPieceHome(squares, brdIdx) {
        let playerNum = squares[brdIdx]
        let homeStart = HOMESTART[playerNum]
        let homeEnd = homeStart + 3
        let placed = false
        for (let homeIdx = homeEnd; homeIdx >= homeStart && !placed; homeIdx--) {
            if (squares[homeIdx] === FILLCHAR) {
                squares[homeIdx] = playerNum
                squares[brdIdx] = FILLCHAR
                placed = true
            }
        }
    }

    /**
     * handleClick is the workhorse that handles user input and updates game state
     *
     * @param int the board index that was clicked
     */
    handleClick(i) {
        const squares = this.state.squares.slice();

        let dieVal = this.state.dieVal;
        let currPlayer = this.state.currPlayer;
        let gstate = this.state.gstate;
        let nextMoves = this.state.nextMoves;
        switch(gstate){
            case GSTATES.ROLL:
                // roll the die
                dieVal = 1 + Math.floor(Math.random() * Math.floor(6));

                // find the next possible moves
                nextMoves = this.showNextMoves(dieVal, currPlayer, squares)
                //  if we have moves go to next state, otherwise next person rolls
                if (nextMoves.size) {
                    gstate = GSTATES.MOVE;
                } else {
                    gstate = GSTATES.SKIP
                }
                break;
            case GSTATES.SKIP:
                dieVal = 0
                currPlayer = (currPlayer % 4) + 1; //  TODO check for disabled players and move this after check for winner
                gstate = GSTATES.ROLL
                break;
            case GSTATES.MOVE:
                //  get i (the brdIdx) and move the piece, but have to get the from and remove the piece
                let toBrdSqr = i;
                let fromBrdSqr = nextMoves.get(i);

                //  check to see if target is occupied and if it is, move it back to owner's home
                if ( squares[toBrdSqr] !== FILLCHAR && squares[toBrdSqr] !== currPlayer) {
                    this.sendPieceHome(squares, toBrdSqr)
                }
                squares[toBrdSqr]   = currPlayer;
                squares[fromBrdSqr] = FILLCHAR

                if (this.calculateWinner(squares)) {
                    gstate = GSTATES.COMPLETE
                } else {
                    currPlayer = (currPlayer % 4) + 1; //  TODO check for disabled players
                    gstate = GSTATES.ROLL
                }

                nextMoves.clear()
                break;
            default:
                break;
        }

        this.setState({
            squares: squares,
            currPlayer: currPlayer,
            dieVal: dieVal,
            gstate: gstate,
            nextMoves: nextMoves
        });
    }  //  end handleClick  ======================================================

    /**
     * render renders the board
     */
    render() {
        const winner = this.calculateWinner()
        let status = winner ? 'Winner!!!' : 'Current player: '

        return(
            <table><tbody>
            <tr><td className="stateTxt" colSpan={ROWCOLCOUNT}>{status}{this.currPlayerSquare(this.state.currPlayer)}&nbsp;&nbsp;&nbsp;&nbsp;{this.renderSkipSquare()}&nbsp;&nbsp;&nbsp;&nbsp;{this.renderDieSquare()}</td></tr>
            <tr><td className="movesTxt" colSpan={ROWCOLCOUNT}>{this.state.nextMoves.size} Moves</td></tr>
            {this.createTable()}
            </tbody>
            </table>
        )
    }  //  end Board.render  =====================================================

    /**
     * calculateWinner tests to see if there is a Winner
     *
     * @param array board squares (might be checked before state update)
     * @return null if no winner, else the Player Number of the winner.
     */
    calculateWinner(squares=this.state.squares) {
        let player = this.state.currPlayer
        let gs     = GOALSTART[player]
        if ( player === squares[gs]   && player === squares[gs+1] &&
             player === squares[gs+2] && player === squares[gs+3] ) {
            return player;
        }
        return null;
    }
}  //  end class Board   =======================================================

/**
 * Game represents the game state and collection of components
*/
class Game extends React.Component {
    render() {
        return(
            <div className="game">
            <div className="game-board" align="center">WAHOO!
            <Board />
            </div>
            <div className="game-info">
            </div>
            </div>
        );
    }
}  //  end class Game  ========================================================

ReactDOM.render(
    <Game />,
    document.getElementById('root')
);
